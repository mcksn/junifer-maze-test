package io.mcksn.jmt.domain;

import java.util.List;

/**
 * Holder for list of positions for available move.
 * Promotes type safety.
 */
public class AvailableMoves {


    private final List<Position> list;

    public AvailableMoves(List<Position> list) {
        this.list = list;
    }

    public List<Position> getAsList() {
        return list;
    }
}