package io.mcksn.jmt.domain;

import com.google.common.annotations.VisibleForTesting;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static io.mcksn.jmt.domain.Content.*;
import static java.lang.Byte.valueOf;
import static java.util.stream.Collectors.toList;

/**
 * Maze created using contents of file.
 */
public class Maze {

    public static final String FILE_DELIMITER = " ";

    public static final int MAZE_END_CONFIG_LINE_NO = 2;

    public static final int MAZE_START_CONFIG_LINE_NO = 1;

    public static final int MAZE_WIDTH_HEIGHT_CONFIG_LINE_NO = 0;
    private final Content[][] array;
    private final List<String> rawFileLines;
    private int width;
    private int height;
    private Position starting;
    private Position ending;
    private Optional<PositionHistory> solution = Optional.empty();

    /**
     * Instantiates a new Maze.
     *
     * @param rawFileLines the raw file lines
     */
    public Maze(List<String> rawFileLines) {
        this.rawFileLines = rawFileLines;
        addWidthAndHeight();
        this.array = new Content[width][height];
    }

    /**
     * Instantiates a new Maze.
     *
     * @param array the array
     */
    Maze(Content[][] array) {
        this.rawFileLines = null;
        this.array = array;
    }

    /**
     * Sets array raw position.
     *
     * @param xAxis      the x axis
     * @param yAxis      the y axis
     * @param rawContent the raw content
     */
    void setArrayRawPosition(int xAxis, int yAxis, int rawContent) {

        if (isStarting(xAxis, yAxis)) {
            array[xAxis][yAxis] = START_FLAG;
        } else if (isEnding(xAxis, yAxis)) {
            array[xAxis][yAxis] = END_FLAG;
        } else {
            array[xAxis][yAxis] = asContent(rawContent);
        }
    }

    private boolean isEnding(int xAxis, int yAxis) {
        return ending.getxAxis() == xAxis && ending.getyAxis() == yAxis;
    }

    private boolean isStarting(int xAxis, int yAxis) {
        return starting.getxAxis() == xAxis && starting.getyAxis() == yAxis;
    }

    /**
     * Gets available moves given previous and current position.
     * <p>
     * Available moves are moves to the left, right, above, below
     * of current position provided it is not outside maze area and not a WALL.
     *
     * @param prePos  the pre pos
     * @param currPos the curr pos
     * @return the available moves
     */
    public AvailableMoves getAvailableMoves(Optional<Position> prePos, Position currPos) {

        AvailableMoves availableMoves = new AvailableMoves(new ArrayList<>(3));

        getPositionFromRaw(currPos.getxAxis() - 1, currPos.getyAxis())
                .ifPresent(availableMoves.getAsList()::add);
        getPositionFromRaw(currPos.getxAxis() + 1, currPos.getyAxis())
                .ifPresent(availableMoves.getAsList()::add);

        getPositionFromRaw(currPos.getxAxis(), currPos.getyAxis() + 1)
                .ifPresent(availableMoves.getAsList()::add);
        getPositionFromRaw(currPos.getxAxis(), currPos.getyAxis() - 1)
                .ifPresent(availableMoves.getAsList()::add);

        prePos.ifPresent(availableMoves.getAsList()::remove);

        List<Position> filteredAvailMoves =
                availableMoves
                        .getAsList()
                        .stream()
                        .filter(p -> p.getContent() != WALL && p.getContent() != START_FLAG)
                        .collect(toList());

        return new AvailableMoves(filteredAvailMoves);
    }

    /**
     * Gets position from raw x axis and y axis.
     *
     * @param xAxisToShift the x axis to shift
     * @param yAxisToShift the y axis to shift
     * @return the position from raw.
     * Returns empty when axis points provided are not in maze area.
     */
    public Optional<Position> getPositionFromRaw(int xAxisToShift, int yAxisToShift) {

        if (0 <= xAxisToShift && xAxisToShift <= width - 1 && 0 <= yAxisToShift && yAxisToShift <= height - 1) {
            Content contentAtPosition = array[xAxisToShift][yAxisToShift];
            return Optional.of(new Position(xAxisToShift, yAxisToShift, contentAtPosition));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Add width and height.
     */
    void addWidthAndHeight() {

        List<String> line1 = Arrays.stream(
                rawFileLines
                        .get(MAZE_WIDTH_HEIGHT_CONFIG_LINE_NO)
                        .split(FILE_DELIMITER))
                .collect(toList());
        this.height = Integer.parseInt(line1.get(0));
        this.width = Integer.parseInt(line1.get(1));

    }

    /**
     * Add starting position.
     */
    void addStartingPosition() {

        List<String> line2 = Arrays.stream(
                rawFileLines
                        .get(MAZE_START_CONFIG_LINE_NO)
                        .split(FILE_DELIMITER))
                .collect(toList());
        starting = new Position(Integer.parseInt(line2.get(1)), Integer.parseInt(line2.get(0)), START_FLAG);
    }

    /**
     * Add ending position.
     */
    void addEndingPosition() {
        List<String> line3 = Arrays.stream(
                rawFileLines
                        .get(MAZE_END_CONFIG_LINE_NO)
                        .split(FILE_DELIMITER))
                .collect(toList());
        ending = new Position(Integer.parseInt(line3.get(1)), Integer.parseInt(line3.get(0)), END_FLAG);
    }

    /**
     * Gets starting position.
     *
     * @return the starting position
     */
    public Position getStartingPosition() {
        return starting;
    }

    /**
     * Build array.
     */
    void buildArray() {

        AtomicInteger xAxis = new AtomicInteger(1);
        AtomicInteger yAxis = new AtomicInteger(1);

        //TODO -1 on yAxis as first increment called before 2nd level mapping
        rawFileLines.stream()
                .skip(3) // config lines
                .forEach(
                        line -> {
                            Arrays.stream(line.split(FILE_DELIMITER))
                                    .forEach(c -> setArrayRawPosition(xAxis.getAndIncrement() - 1, yAxis.get() - 1, valueOf(c)));
                            yAxis.getAndIncrement(); //next row
                            xAxis.set(1); //back to fist column
                        } //row
                ); //column

    }


    /**
     * Build.
     */
    public void build() {

        addWidthAndHeight();
        addStartingPosition();
        addEndingPosition();
        buildArray();

    }

    /**
     * Sets solution.
     *
     * @param positionHistory the position history
     */
    public void setSolution(PositionHistory positionHistory) {
        this.solution = Optional.of(positionHistory);
    }

    /**
     * Gets solution.
     *
     * @return the solution
     */
    public Optional<PositionHistory> getSolution() {
        return solution;
    }

    /**
     * Print maze to output stream.
     *
     * @param outputStream the output stream
     */
    public void printTo(OutputStream outputStream) {
        PrintStream ps = new PrintStream(outputStream);

        for (int y = 0; y < height; y++) {
            ps.println();
            for (int x = 0; x < width; x++) {
                ps.print(asPrintableContent(x, y));
            }
        }
    }

    /**
     * Returns printable character given axis points.
     * @param xAxis
     * @param yAxis
     * @return
     */
    private String asPrintableContent(int xAxis, int yAxis) {
        String rtnCharacter = null;

        if (array[xAxis][yAxis] == EMPTY) {
            rtnCharacter = " ";
        }

        if (array[xAxis][yAxis] == WALL) {
            rtnCharacter = "#";
        }

        if (solution.get().getAsList().contains(new Position(xAxis, yAxis, array[xAxis][yAxis]))) {
            rtnCharacter = "X";
        }

        if (array[xAxis][yAxis] == START_FLAG) {
            rtnCharacter = "S";
        }

        if (array[xAxis][yAxis] == END_FLAG) {
            rtnCharacter = "E";
        }
        return rtnCharacter;
    }

    /**
     * Sets width.
     *
     * @param width the width
     */
    @VisibleForTesting
    void setWidth(int width) {
        this.width = width;
    }

    /**
     * Sets height.
     *
     * @param height the height
     */
    @VisibleForTesting
    void setHeight(int height) {
        this.height = height;
    }
}