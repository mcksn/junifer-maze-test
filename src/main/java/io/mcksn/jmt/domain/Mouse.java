package io.mcksn.jmt.domain;

import java.util.ArrayList;
import java.util.Optional;

import static io.mcksn.jmt.domain.Content.END_FLAG;

/**
 * Represents a token with the ability to move through the maze.
 */
public class Mouse {

    private final Maze maze;
    private Position currentPos;
    private Optional<Position> previousPos;
    private final PositionHistory positionHistory;

    public Mouse(Maze maze, Position startingPosition, Optional<Position> previousPos) {
        this.maze = maze;
        this.currentPos = startingPosition;
        this.previousPos = previousPos;
        this.positionHistory = new PositionHistory(new ArrayList<>());
    }

    public Mouse(Maze maze, PositionHistory history, Position startingPosition, Optional<Position> previousPos) {
        this.maze = maze;
        this.currentPos = startingPosition;
        this.previousPos = previousPos;
        this.positionHistory = new PositionHistory(new ArrayList<>(history.getAsList()));
    }

    /**
     * Sen mouse to travel through maze in
     * order to find the solution.
     */
    public void travel() {

        if(maze.getSolution().isPresent())
        {
            //stop trying to solve
            return;
        }

        positionHistory.getAsList().add(currentPos);

        if (currentPos.getContent() == END_FLAG) {
            maze.setSolution(positionHistory);
            return;
        }

        AvailableMoves moves = this.maze.getAvailableMoves(previousPos, currentPos);

        if (moves.getAsList().size() == 1) {
            this.previousPos = Optional.of(currentPos);
            this.currentPos = moves.getAsList().get(0);
            this.travel();
        } else {
            sendClonesDownEachBranch(moves);
        }
    }

    private void sendClonesDownEachBranch(AvailableMoves availableMoves) {

        for (Position pos : availableMoves.getAsList()) {

            Mouse clone = new Mouse(this.maze, this.positionHistory, pos, Optional.of(this.currentPos));
            clone.travel();

        }
    }

}
