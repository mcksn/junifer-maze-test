package io.mcksn.jmt.domain;

import com.google.common.base.Objects;

import static com.google.common.base.Objects.toStringHelper;


/**
 * Position within a maze.
 */
public class Position {
    private final int xAxis;
    private final int yAxis;
    private final Content content;

    /**
     * Instantiates a new Position.
     *
     * @param xAxis   the x axis
     * @param yAxis   the y axis
     * @param content the content
     */
    public Position(int xAxis, int yAxis, Content content) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.content = content;
    }

    /**
     * Gets x axis point.
     *
     * @return the axis
     */
    public int getxAxis() {
        return xAxis;
    }

    /**
     * Gets y axis.
     *
     * @return the axis
     */
    public int getyAxis() {
        return yAxis;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public Content getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return xAxis == position.xAxis &&
                yAxis == position.yAxis &&
                content == position.content;
    }

    @Override
    public int hashCode() {
        return java.util.Objects.hash(xAxis, yAxis, content);
    }

    @Override
    public String toString() {
        return toStringHelper(this)
                .add("xAxis", xAxis)
                .add("yAxis", yAxis)
                .add("content", content)
                .toString();
    }
}
