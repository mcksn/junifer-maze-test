package io.mcksn.jmt.domain;

/**
 * Content of a maze area.
 */
public enum Content {
    //TODO Create underlying datatype which includes printable character
    START_FLAG, END_FLAG, EMPTY, WALL;

    public static Content asContent(int i) {
        switch (i) {
            case 1:
                return WALL;
            case 0:
                return EMPTY;
        }
        //TODO with exception
        return null;
    }
}
