package io.mcksn.jmt.domain;

import java.util.List;

/**
 * Holder for path or  position history.
 * Promotes type safety.
 */
public class PositionHistory {


    private final List<Position> list;

    public PositionHistory(List<Position> list) {
        this.list = list;
    }

    public List<Position> getAsList() {
        return list;
    }
}