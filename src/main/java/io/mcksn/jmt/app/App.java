package io.mcksn.jmt.app;

import com.google.common.annotations.VisibleForTesting;
import io.mcksn.jmt.domain.Maze;
import io.mcksn.jmt.domain.Mouse;
import io.mcksn.jmt.domain.PositionHistory;

import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class App {

    private final OutputStream outStream;
    private final String mazeFileURI;
    private Optional<PositionHistory> solution;


    @VisibleForTesting
    App(String mazeFileURI, OutputStream outStream) {

        this.mazeFileURI = mazeFileURI;
        this.outStream = outStream;

    }

    public static void main(String... args) throws IOException, ExecutionException, InterruptedException {

        new App(args[0], System.out).solve();

    }

    @VisibleForTesting
   void solve() throws IOException {

        InputStream inputStream = App.class.getClassLoader().getResourceAsStream(mazeFileURI);
        BufferedReader br = new
                BufferedReader(new InputStreamReader(inputStream));

        List<String> fileLines = br.lines().collect(Collectors.toList());

        Maze maze = new Maze(fileLines);
        maze.build();

        Mouse mouse = new Mouse(maze, maze.getStartingPosition(), Optional.empty());
        mouse.travel();

        solution = maze.getSolution();

        maze.printTo(outStream);

    }

    @VisibleForTesting
    PositionHistory getSolution() {
        return solution.get();
    }
}
