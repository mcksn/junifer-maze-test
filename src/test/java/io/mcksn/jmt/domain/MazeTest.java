package io.mcksn.jmt.domain;

import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

/**
 * Unit tests for Maze.
 */
public class MazeTest {

    @Test
    public void testGetAvailableMovesFiltersPrev() throws Exception {

        //Given
        Content[][] content = new Content[2][2];
        content[0][0] = Content.EMPTY;
        content[0][1] = Content.EMPTY;
        content[1][0] = Content.EMPTY;
        content[1][1] = Content.EMPTY;
        Maze maze = new Maze(content);
        maze.setHeight(2);
        maze.setWidth(2);
        Optional<Position> prev = Optional.of(new Position(0, 0, Content.EMPTY));
        Position curr = new Position(1, 0, Content.EMPTY);

        //When
        AvailableMoves availableMoves = maze.getAvailableMoves(prev, curr);

        //Then
        assertThat(availableMoves.getAsList(),
                containsInAnyOrder(
                        new Position(1, 1, Content.EMPTY)));

    }

    @Test
    public void testGetAvailableMovesFiltersWalls() throws Exception {

        //Given
        Content[][] content = new Content[2][2];
        content[0][0] = Content.EMPTY;
        content[0][1] = Content.WALL;
        content[1][0] = Content.EMPTY;
        content[1][1] = Content.EMPTY;
        Maze maze = new Maze(content);
        maze.setHeight(2);
        maze.setWidth(2);
        Optional<Position> prev = Optional.empty();
        Position curr = new Position(0, 0, Content.EMPTY);

        //When
        AvailableMoves availableMoves = maze.getAvailableMoves(prev, curr);

        //Then
        assertThat(availableMoves.getAsList(),
                containsInAnyOrder(
                        new Position(1, 0, Content.EMPTY)));

    }

    @Test
    public void testGetAvailableMovesWithNoNeedToFilter() throws Exception {

        //Given
        Content[][] content = new Content[2][2];
        content[0][0] = Content.EMPTY;
        content[0][1] = Content.EMPTY;
        content[1][0] = Content.EMPTY;
        content[1][1] = Content.EMPTY;
        Maze maze = new Maze(content);
        maze.setHeight(2);
        maze.setWidth(2);
        Optional<Position> prev = Optional.empty();
        Position curr = new Position(0, 0, Content.EMPTY);

        //When
        AvailableMoves availableMoves = maze.getAvailableMoves(prev, curr);

        //Then
        assertThat(availableMoves.getAsList(),
                containsInAnyOrder(
                        new Position(1, 0, Content.EMPTY),
                        new Position(0, 1, Content.EMPTY)));

    }
}