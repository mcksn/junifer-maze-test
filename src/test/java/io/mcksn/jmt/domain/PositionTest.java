package io.mcksn.jmt.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

/**
 * Unit tests for Position.
 */
public class PositionTest {

    @Test
    public void testEqualsAndHashCode() throws Exception {
        EqualsVerifier.forClass(Position.class).usingGetClass().verify();
    }
}