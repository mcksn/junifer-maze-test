package io.mcksn.jmt.domain;

import org.junit.Test;

import static io.mcksn.jmt.domain.Content.EMPTY;
import static io.mcksn.jmt.domain.Content.WALL;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Unit tests for Content.
 */
public class ContentTest {

    @Test
    public void testAsContentWALL() throws Exception {

        //Given //When //Then
        assertThat(Content.asContent(1), is(WALL));
    }

    @Test
    public void testAsContentEMPTY() throws Exception {

        //Given //When //Then
        assertThat(Content.asContent(0), is(EMPTY));
    }
}