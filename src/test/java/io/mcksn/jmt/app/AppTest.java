package io.mcksn.jmt.app;

import io.mcksn.jmt.domain.Content;
import io.mcksn.jmt.domain.Position;
import io.mcksn.jmt.domain.PositionHistory;
import org.junit.Test;

import java.io.ByteArrayOutputStream;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Integration tests using test files supplied.
 */
public class AppTest {

    @Test
    public void testSolveMazeWithNoBranches() throws Exception {

        //Given
        App app = new App("maze/input.txt", System.out);

        //When
        app.solve();
        PositionHistory actualPosHistory = app.getSolution();

        //Then
        assertThat(actualPosHistory.getAsList(),
                containsInAnyOrder(
                        new Position(1, 1, Content.START_FLAG)
                        , new Position(1, 2, Content.EMPTY)
                        , new Position(1, 3, Content.EMPTY)
                        , new Position(2, 3, Content.EMPTY)
                        , new Position(3, 3, Content.END_FLAG)));

    }

    @Test
    public void testPrintSolution() throws Exception {

        //Given
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        App app = new App("maze/input.txt", output);

        //When
        app.solve();

        //Then
        assertThat("printed solution not as expected", output.toString(), equalTo(
                    "\r\n#####\r\n" +
                        "#S# #\r\n" +
                        "#X# #\r\n" +
                        "#XXE#\r\n" +
                        "#####"));
    }

    @Test
    public void testSolveMazeWithOneBranch() throws Exception {

        //Given
        App app = new App("maze/small.txt", System.out);

        //When
        app.solve();
        PositionHistory actualPosHistory = app.getSolution();

        //Then
        assertThat(actualPosHistory.getAsList(),
                contains(
                        new Position(1, 1, Content.START_FLAG),
                        new Position(2, 1, Content.EMPTY),
                        new Position(3, 1, Content.EMPTY),
                        new Position(3, 2, Content.EMPTY),
                        new Position(3, 3, Content.EMPTY),
                        new Position(3, 4, Content.END_FLAG)));
    }

    @Test
    public void testSolveMazeWithOnlyBorderWalls() throws Exception {

        //Given
        App app = new App("maze/sparse_medium.txt", System.out);

        //When
        app.solve();
        PositionHistory actualPosHistory = app.getSolution();

        //Then
        assertTrue("End flag not in position history",
                actualPosHistory.getAsList().contains(new Position(19, 19, Content.END_FLAG)));
    }


    @Test
    public void testSolveMazeWithMultipleBranches2() throws Exception {

        //Given
        App app = new App("maze/medium_input.txt", System.out);

        //When
        app.solve();
        PositionHistory actualPosHistory = app.getSolution();

        //Then
        assertTrue("End flag not in position history",
                actualPosHistory.getAsList().contains(new Position(19, 19, Content.END_FLAG)));

    }

    @Test
    public void testSolveMazeWithMultipleBranches() throws Exception {

        //Given
        App app = new App("maze/large_input.txt", System.out);

        //When
        app.solve();
        PositionHistory actualPosHistory = app.getSolution();

        //Then
        assertTrue("End flag not in position history",
                actualPosHistory.getAsList().contains(new Position(159, 99, Content.END_FLAG)));

    }

}